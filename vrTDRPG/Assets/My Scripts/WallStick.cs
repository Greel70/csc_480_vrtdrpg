﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallStick : MonoBehaviour {
    

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnCollisionStay(Collision c)
    {
        Debug.Log(c.collider.tag);
        if (c.collider.tag == "target")
        {
            GetComponent<Rigidbody>().isKinematic = true;
        }
        
    }
    void OnCollisionExit(Collision c)
    {

        if (c.collider.tag == "target")
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }

    }
}
